# FAP-Alike Webform Project

FAP-Alike project is written with C#.NET and developed based on the website [fap.fpt.edu.vn](http://fap.fpt.edu.vn/), a site allows students to view courses, their weekly timetable, attendance status, daily news... at academia.

## Notes

*  This project must be opened and run by Visual Studio 2015+ IDE.
*  Run ***database_script.sql*** by SQL Server Management Studio and execute it to set up database for the application.

## Detailed information to experience the system

*  Account details **(to log in the system, choose campus "FU-Hòa Lạc" at drop-down-list and enter below informations)**

        username  |  password
        --------------------
        he130022  |  123
        he130069  |  123
        he130088  |  123
        he130059  |  123
        he130369  |  123
        
*  At homepage, there are some features:
    -   10 recent university's news are at the left of page, you could click on title to view article's details.
    -   Click on **University timetable** of **Information Access** heading to view all courses in academia.
    -   Click on **student code** at the top-right of page (e.g: **huypqhe130022**) to view detailed information of logged in user.
    -   Click on **Weekly timetable** of **Information Access** heading to view weekly timetable of logged in student.
    -   Click on **View exam schedule** of **Information Access** heading to view exam schedule of logged in student.
    -   Click on **Attendance report** of **Reports** heading to view attendance report of logged in student.
    -   Click on **Update Profile** of **Others** heading to edit informations of logged in student.